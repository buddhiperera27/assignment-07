#include <stdio.h>
#include <string.h>

int main()
{
   char frequencyString[100];
   int charCount = 0, countArray[26] = {0}, value;

   printf("Enter a string in simple letters\n ex: hello \n");
   gets(frequencyString);

   while (frequencyString[charCount] != '\0') {
  
      if (frequencyString[charCount] >= 'a' && frequencyString[charCount] <= 'z') {
         value = frequencyString[charCount] - 'a';
         countArray[value]++;
      }

      charCount++;
   }

   for (charCount = 0; charCount < 26; charCount++)
         printf("%c character has %d times of frequency in the string you entered.\n", charCount + 'a', countArray[charCount]);

   return 0;
}