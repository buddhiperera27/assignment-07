#include <stdio.h>

void convertToReverse();

int main() {

    printf("Enter the sentence you want to convert to reverse : ");

    convertToReverse();

    return 0;

}

void convertToReverse() {

    char sentence;

    scanf("%c", &sentence);

    if (sentence != '\n') {

        convertToReverse();

        printf("%c", sentence);
    }
}