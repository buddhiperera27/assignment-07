#include<stdio.h>

int main(void)
{
	int a[3][3], b[3][3], c[3][3]={0}, d[3][3]={0};
	int matrixARows, matrixACols, matrixBRows, matrixBCols;

	printf("Enter number of rows of Matrix A: ");
	scanf("%d", &matrixARows);
	printf("Enter number of columns of Matrix A: ");
	scanf("%d", &matrixACols);

  printf("\n");

	printf("Enter number of rows of Matrix B: ");
	scanf("%d", &matrixBRows);
	printf("Enter number of columns of Matrix B: ");
	scanf("%d", &matrixBCols);

	printf("\n");

	if(matrixACols != matrixBRows)
	{
		printf("Error!! Cannot do the Matrix Multiplication");
		return -1;
	}
	else
	{
		printf("Matrix A: \n");

		for(int i = 0; i < matrixARows; i++){
      for(int j = 0; j < matrixACols; j++){
        printf("Enter A %d , %d : ", i + 1, j + 1);
        scanf("%d", &a[i][j]);
      }
		}

    printf("\n");

	  printf("Matrix B: \n");
	  for(int i = 0; i < matrixBRows; i++){
      for(int j = 0; j < matrixBCols; j++){
        printf("Enter B %d , %d : ", i + 1, j + 1);
          scanf("%d", &b[i][j]);
      }
		}

		//Addition
		for(int i = 0; i < matrixARows ; i++){
      for(int j = 0; j < matrixACols; j++){
        c[i][j] = a[i][j] + b[i][j];
      }
		}

		printf("\nMatrix Addition:\n");

		for(int i = 0; i < matrixARows ; i++){
			for(int j = 0; j < matrixACols; j++){
        printf("%d ", c[i][j]);
			}
			printf("\n");
		}

		//Multiplication
		for(int i = 0; i < matrixARows; i++){
      for(int j = 0; j < matrixBCols; j++){
        for(int k = 0; k < matrixBRows; k++){
          d[i][j] += a[i][k] * b[k][j];
        }
      }
		}

		printf("\nMatrix Multiplication:\n");

		for(int i = 0; i < matrixARows; i++){
			for(int j = 0; j < matrixBCols; j++){
        printf("%d ", d[i][j]);
			}
			printf("\n");
		}
	}
  return 0;
}